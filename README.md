# README #

This is the .exe file for the Laser League Bingo.  

Download from Bitbucket. 
Run the .exe file.
You should now have a file called "LaserLeagueBingo.txt".  This is in the format needed for https://bingosync.com/
Copy all the text and paste it in a "Custom (Advanced)" Game in bingosync.  
Enjoy the bingo!